// get JSON file
fetch('./data/work.json')
    .then(response => response.json())
    .then(data => {
        localStorage.setItem('images', JSON.stringify(data))
    });

const workGallery = document.querySelector('.work-gallery');
const galleryMenu = document.querySelector('.work-tabs');
const loadBtn = document.querySelector('#gallery-btn');
const preloader = document.querySelector('#work-loader');
const imageCollection = JSON.parse(localStorage.getItem('images')).images;

let randomList = generateRandomNumbers();
let imagesCount = 0;
let currentType = 'all';

window.addEventListener('scroll', function scroll() {
    if (isInViewPort(workGallery)) {
        window.removeEventListener('scroll', scroll);
        showRandomImages(randomList);
    }
});

galleryMenu.addEventListener('click', (e) => {
    const currentTab = e.target;
    currentType = currentTab.dataset.type;
    imagesCount = 0;
    if (currentTab.classList.contains('work-title')) {
        changeTab(currentTab);
        const currentCollection = filterCollection(currentType);
        if (currentType !== 'all') {
            loadBtn.classList.add('hidden');
            removeImages(currentType);
            showImages(currentCollection);
        } else {
            loadBtn.classList.remove('hidden');
            removeImages();
            showRandomImages(randomList);
        }
    }
});

loadBtn.addEventListener('click', () => {
    if (currentType === 'all') {
        toggleBtn();
        setTimeout(() => {
            showRandomImages(randomList);
            toggleBtn();
        }, 2000);
    }
});

function filterCollection(type) {
    return imageCollection.filter(image => image.type === type)
}

function toggleBtn() {
    loadBtn.classList.toggle('hidden');
    preloader.classList.toggle('hidden');
    if (imagesCount >= randomList.length) {
        loadBtn.classList.add('hidden');
    }
}

function changeTab(tab) {
    [...galleryMenu.children].forEach(tab => tab.classList.remove('work-title_active'));
    tab.classList.add('work-title_active');
}

function showRandomImages(randomInt) {
    let localCount = 0;

    randomInt.slice(imagesCount, imagesCount + 12).forEach(int => {
        imagesCount++;
        localCount++;
        if (imagesCount <= randomList.length && localCount <= 12) {
            const imageParameters = imageCollection[int];
            appendImage(imageParameters)
        }
    })
}

function showImages(collection) {
    collection.forEach(item => {
        if (!item.isActive) {
            const imageParameters = item;
            appendImage(imageParameters)
        }
    })
}

function appendImage(parameters) {
    const timeout = Math.round(Math.random() * 2000);
    const image = createElement(parameters);

    image.style.animationDelay = `${timeout}ms`;
    workGallery.append(image);
    parameters.isActive = true
}

function removeImages(type) {
    const currentImages = workGallery.querySelectorAll('.gallery-container');
    // if no type in arguments, it means, that current type of images is 'all'
    if (!type) {
        currentImages.forEach(container => container.remove());
        imageCollection.forEach(image => image.isActive = false)
    } else {
        imageCollection.filter(image => image.type !== type).forEach(image => image.isActive = false);

        [...currentImages].filter(image => image.dataset.type !== type).forEach(image => image.remove())
    }
}

// getting random numbers to get random images, each time, when loading 'all' category
function generateRandomNumbers() {
    const numbersList = [];
    while (numbersList.length < 36) {
        const int = Math.floor(Math.random() * imageCollection.length);
        if (numbersList.indexOf(int) === -1) {
            numbersList.push(int)
        }
    }
    return numbersList
}

function isInViewPort(elem) {
    const bounding = elem.getBoundingClientRect();
    return (
        bounding.top <= window.innerHeight && bounding.bottom >= 0
    );
}


// DOM element constructor
function createElement({src, type, description, alt}) {
    const container = document.createElement('div');
    const overlay = document.createElement('div');
    const image = document.createElement('img');
    const buttonSet = document.createElement('div');
    const heading = document.createElement('h4');
    const subheading = document.createElement('span');

    container.classList.add('gallery-container');
    container.dataset.type = type;

    image.classList.add('gallery-img');
    image.alt = alt;
    image.src = src;

    overlay.classList.add('gallery-overlay');
    buttonSet.classList.add('overlay-buttons');
    buttonSet.innerHTML = `<button class="overlay-btn">
                                <i class="btn-icon fa fa-link"></i>
                            </button>
                            <button class="overlay-btn">
                                 <i class="btn-icon fa fa-search"></i>
                            </button>`;

    heading.classList.add('overlay-heading');
    heading.innerText = description;

    subheading.classList.add('overlay-subheading');
    subheading.innerText = alt;
    overlay.append(buttonSet, heading, subheading);
    container.append(image, overlay);
    return container
}

